var express = require('express');  // módulo express
var app = express();		   // objeto express
var bodyParser = require('body-parser');  // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var irc = require('irc');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));
var path = require('path');	// módulo usado para lidar com caminhos de arquivos
var proxies = {}; // mapa de proxys
var proxy_id = 0;
function proxy(id, servidor, nick, canal) {
	var cache = []; // cache de mensagens
	var irc_client = new irc.Client(
			servidor,
			nick,
			{channels: [canal],});

	irc_client.addListener('message'+canal, function (from, message) {
	    console.log(from + ' => '+ canal +': ' + message);
	    cache.push(	{"timestamp":Date.now(),
			"nick":from,
			"msg":message} );
	});

	irc_client.addListener('whois', function(info) {
		//console.log('whois results ' +  );
	});
	irc_client.addListener('error', function(message) {
	    console.log('error: ', message);
	});
	irc_client.addListener('nick', function(oldnick, newnick, channels, message) {
		console.log('NICK: ' + oldnick + ' changes nick to ' + newnick);
	});

	irc_client.addListener('quit', function(nick, reason, channels, message) {
		console.log(nick +' has quit: '+reason);
	});
	irc_client.addListener('join', 	function (channel, nick, message) {
		console.log(nick +' has joined the channel '+ channel);
	});
	
	irc_client.addListener('pm', function(nick, message) {
    		console.log('Got private message from %s: %s', nick, message);
		    cache.push(	{"timestamp":Date.now(),
			"nick":nick,
			"msg":"[PRIVMSG] "+message} );
	});

	irc_client.addListener('names', function(channel, nicks)
	{
		var lista = channel+JSON.stringify(nicks);
		console.log('listagem de nicks do canal: '+lista);
		//client.say(canal, "lista de nicks do canal: "+lista);
	});
	proxies[id] = { "cache":cache, "irc_client":irc_client  };
	return proxies[id];
}
app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal) {
	proxy_id++;
	var p =	proxy(	proxy_id,
			req.cookies.servidor,
			req.cookies.nick,
			req.cookies.canal);
	res.cookie('id', proxy_id);
  	res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
        res.sendFile(path.join(__dirname, '/login.html'));
  }
});

app.get('/obter_mensagem/:timestamp', function (req, res) {
  var id = req.cookies.id;
  res.append('Content-type', 'application/json');
  res.send(proxies[id].cache);
});

app.get('/rest/:comando', function (req, res) {
	var cmd = req.params.comando;
	var irc_client = proxies[req.cookies.id].irc_client;
				switch(cmd){
					case "quit":
						//console.log('QUIT 1');
						irc_client.emit("quit", req.cookies.nick, req.cookies.canal);
						irc_client.disconnect();
						res.end();
					break;

					case "part":
						irc_client.send("part", req.cookies.canal, req.cookies.nick);
						if (irc_client.opt.channels.length == 1) // quit
						{
							irc_client.emit("quit", req.cookies.nick, req.cookies.canal);
							irc_client.disconnect();
							res.send("0");
						}
						else {
							irc_client.opt.channels[0] = irc_client.opt.channels[irc_client.opt.channels.length-1];
							irc_client.opt.channels.pop();
							res.send(irc_client.opt.channels[0]);
						}

					break;


					default:
						console.log("Comando não encontrado");
				}
});


app.get('/rest/:comando/:args', function (req, res) {
	var cmd = req.params.comando;
	var arg = req.params.args;
	var irc_client = proxies[req.cookies.id].irc_client;
  		 switch(cmd){
   				case "nick":
	       			 irc_client.send("nick", arg);
           			 irc_client.emit("nick", req.cookies.nick, arg, req.cookies.canal);
					 res.send(arg);
				break;

				case "join":
						arg = "#" + arg;
						irc_client.join(arg);
						irc_client.send("join", arg);
						irc_client.emit("join", arg, req.cookies.nick);
						var current_channel = irc_client.opt.channels[0];
						irc_client.opt.channels[0] = arg;
						irc_client.opt.channels.push(current_channel);
						res.send(arg);
				break;

				case "whois":
			 			irc_client.whois(arg);
						irc_client.emit("whois", arg);
						res.end();
				break;
				
				case "privmsg":
				var nome = "";
				var msg = "";
					for(var i = 0; i < arg.length; i++){
						if(arg[i] != " "){
							nome+=arg[i];
						}
						else{
							msg = arg.substr(i+1);
							break;
						}
					}
					irc_client.say(nome, msg);
				break;

				default:
						console.log("Comando não encontrado");
        }
});

app.post('/gravar_mensagem', function (req, res) {
  proxies[req.cookies.id].cache.push(req.body);
  var irc_client = proxies[req.cookies.id].irc_client;
  irc_client.say(req.cookies['canal'], req.body.msg);
  res.end();
});
app.post('/login', function (req, res) {
   res.cookie('nick', req.body.nome);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
