/*
  Adiciona <mensagem> no <elemento_id>.
*/
function adiciona_mensagem(mensagem,elemento_id,timestamp) {
	var novo_elemento = document.createElement('div');
	novo_elemento.id = "mensagem"+timestamp;
	document.getElementById(elemento_id).appendChild(novo_elemento);
	document.getElementById('mensagem'+timestamp).innerHTML=mensagem;
}

/*
  Adiciona <canal> no seletor de canais (id = canal).
*/
function adiciona_canal(canal) {
	var x = document.getElementById("canal");
  var option = document.createElement("option");
  option.text = canal;
	option.selected = 'selected';
	option.id = canal;
  x.add(option);
}

/*
  Remove <canal> no seletor de canais (id = canal).
*/
function remove_canal() {
	var x = document.getElementById("canal");
  x.remove(x.selectedIndex);
}

/*
  Altera o <canal> no qual se lê e envia mensagens.
*/
function altera_canal() {
	var x = document.getElementById("canal");
	var selectedValue = x.options[x.selectedIndex].value;
	Cookies.set("canal", selectedValue);
	$("#status").text("Conectado - irc://"+
			Cookies.get("nick")+"@"+
			Cookies.get("servidor")+"/"+
			Cookies.get("canal"));
}

/*
  Transforma timestamp em formato HH:MM:SS
*/
function timestamp_to_date( timestamp ) {
	var date = new Date( timestamp );
	var hours = date.getHours();
	var s_hours = hours < 10 ? "0"+hours : ""+hours;
	var minutes = date.getMinutes();
	var s_minutes = minutes < 10 ? "0"+minutes : ""+minutes;
	var seconds = date.getSeconds();
	var s_seconds = seconds < 10 ? "0"+seconds : ""+seconds;
	return s_hours + ":" + s_minutes + ":" + s_seconds;
}

function iniciar(elemento_id) {
	$("#status").text("Conectado - irc://"+
			Cookies.get("nick")+"@"+
			Cookies.get("servidor")+"/"+
			Cookies.get("canal"));
	carrega_mensagens(elemento_id,0);
	carrega_usuarios();
	adiciona_canal(Cookies.get("canal"));
}


function carrega_usuarios()
{
	var usuario = "usuario";
	var novo_elemento = document.createElement('div');
	novo_elemento.id = usuario;
	document.getElementById("usuarios").appendChild(novo_elemento);
	document.getElementById(usuario).innerHTML=usuario;
}

/*
  Carrega as mensagens ocorridas após o <timestamp>,
  acrescentando-as no <elemento_id>
*/
var novo_timestamp="0";
function carrega_mensagens(elemento_id, timestamp) {
	var mensagem = "";
	var horario = "";
	$.get("obter_mensagem/"+timestamp, function(data,status) {
		if ( status == "success" ) {
		    var linhas = data;
		    for ( var i = linhas.length-1; i >= 0; i-- ) {
		    	horario = timestamp_to_date(linhas[i].timestamp);
			mensagem =
				"["+horario+" - "+
				linhas[i].nick+"]: "+
		                linhas[i].msg;
			novo_timestamp = linhas[i].timestamp;
		    	adiciona_mensagem(mensagem,elemento_id,novo_timestamp);
			}
		}
		else {
		    alert("erro: "+status);
		}
		}
	);
	t = setTimeout(
		function() {
			carrega_mensagens(elemento_id,novo_timestamp)
		},
		100);
}

/*
     Identifica se o texto recebido é uma mensagem ou um comando IRC
*/
function identifica_texto(elem_id_mensagem){
var texto = document.getElementById(elem_id_mensagem).value;
	if(texto.startsWith("/")){
		var comando = "";
		var arg = "";
			for(var i = 1; i < texto.length; i++){
				if(texto[i] != " ")
					comando+=texto[i];
				else {
					arg = texto.substr(i+1);
				break;
				}
			}


		if(arg == ""){
			$.get("rest/"+comando+"/", function(data,status) {
				if(status != "success")
					alert("erro "+status);

				if(comando == "quit")
				{
					Cookies.set('nick', "");
					Cookies.set('canal', "");
					Cookies.set('servidor', "");
					location.reload();
				}

				if(comando == "part")
				{
					if (data == "0")
					{
						Cookies.set('canal', "");
						location.reload();
					}
					else {
						Cookies.set('canal', data);
						$("#status").text("Conectado - irc://"+
								Cookies.get("nick")+"@"+
								Cookies.get("servidor")+"/"+
								Cookies.get("canal"));
						remove_canal();
						carrega_mensagens(elemento_id,0);
						carrega_usuarios();

					}
				}
			});
		} else {
		   	$.get("rest/"+comando+"/"+arg+"", function(data,status) {
				if(status != "success")
					alert("erro "+status);

				if(comando == "nick")
				{
					Cookies.set('nick', data);
					iniciar("mensagem");
				}

				if(comando == "join")
				{
					Cookies.set('canal', data);
					iniciar("mensagem");
				}
			});
		}
	} else {
	   submete_mensagem(elem_id_mensagem);
	}
	// limpa a caixa de texto apos enviar a mensagem
	document.getElementById(elem_id_mensagem).value = "";
}


/*
   Submete a mensagem dos valores contidos s elementos identificados
   como <elem_id_nome> e <elem_id_mensagem>
*/
function submete_mensagem(elem_id_mensagem) {
	var mensagem= document.getElementById(elem_id_mensagem).value;

	var msg = '{"timestamp":'+Date.now()+','+
		  '"nick":"'+Cookies.get("nick")+'",'+
                  '"msg":"'+mensagem+'"}';

	$.ajax({
		type: "post",
		url: "/gravar_mensagem",
		data: msg,
		success:
		function(data,status) {
			if (status == "success") {
			    // nada por enquanto
			}
			else {
 				alert("erro: "+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
		});
}
